
function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    let result = {}
    try {
        for (let keys in obj) {
            result[obj[keys]] = keys
        }
    } catch (error) {
        console.log(error)
    }
    return result;
}

module.exports = invert