
function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    try {
        for (let keys in defaultProps) {
            if (obj[keys] === undefined) {
                obj[keys] = defaultProps[keys]
            }
        }
        return obj
    } catch (error) {
        console.log(error)
    }
}

module.exports = defaults