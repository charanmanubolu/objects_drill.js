function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    let result = []
    try {
        for (let keys in obj) {
            result.push(obj[keys])
        }
    } catch (error) {
        console.log(error)
    }
    return result
}

module.exports = values