function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    let result = []
    try {
        for (let keys in obj) {
            result.push(keys)
        }
    } catch (error) {
        console.log(error)
    }
    return result
}

module.exports=keys