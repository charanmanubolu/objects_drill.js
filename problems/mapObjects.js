function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    let result = {}
    try {
        for (let keys in obj) {
            let values = obj[keys]
            result[keys] = cb(values)
        }
    } catch (error) {
        console.log(error);
    }
    return result
}

module.exports = mapObject;