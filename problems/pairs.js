
function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    let result = []
    try {
        for (let keys in obj) {
            result.push([keys, obj[keys]])
        }
    } catch (error) {
        console.log(error)
    }
    return result
}

module.exports = pairs